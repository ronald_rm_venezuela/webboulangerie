package com.webBoulangerie.boulangerie;

import java.net.URI;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import com.webBoulangerie.boulangerie.data.entity.Receta;

public class Rest {

    public void getRecetaById() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/user/receta/{id}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Receta> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Receta.class, 1);
        Receta receta = responseEntity.getBody();
        System.out.println("Id:"+receta.getId()+", Title:"+receta.getPesoUnitario()
                +", Category:"+receta.getNombre());
    }
    public void getAllReceta() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/user/receta";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Receta[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Receta[].class);
        Receta[] recetas = responseEntity.getBody();
        for(Receta receta : recetas) {
            System.out.println("Id:"+receta.getId()+", Peso Unitario:"+receta.getPesoUnitario() +", Nombre: "+receta.getNombre());
        }
    }
    public void addReceta() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/user/receta";
        Receta objReceta = new Receta();
        objReceta.setPesoUnitario((float) 10.20);
        objReceta.setNombre("Spring");
        HttpEntity<Receta> requestEntity = new HttpEntity<Receta>(objReceta, headers);
        URI uri = restTemplate.postForLocation(url, requestEntity);
        System.out.println(uri.getPath());
    }
    public void updateReceta() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/user/receta";
        Receta objReceta = new Receta();
        objReceta.setId(1);
        objReceta.setPesoUnitario((float) 10.2);
        objReceta.setNombre("Java");
        HttpEntity<Receta> requestEntity = new HttpEntity<Receta>(objReceta, headers);
        restTemplate.put(url, requestEntity);
    }
    public void deleteReceta() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/user/receta/{id}";
        HttpEntity<Receta> requestEntity = new HttpEntity<Receta>(headers);
        restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class, 4);
    }
    public static void main(String args[]) {
        Rest util = new Rest();
        //util.getArticleByIdDemo();
        //util.addArticleDemo();
        //util.updateArticleDemo();
        //util.deleteArticleDemo();
        util.getAllReceta();
    }
}

