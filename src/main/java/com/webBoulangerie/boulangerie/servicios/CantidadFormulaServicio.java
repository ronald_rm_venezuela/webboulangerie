package com.webBoulangerie.boulangerie.servicios;

import com.webBoulangerie.boulangerie.data.entity.CantidadFormula;
import com.webBoulangerie.boulangerie.repositorios.CantidadFormulaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.ArrayList;

@Service
public class CantidadFormulaServicio {

    @Autowired
    CantidadFormulaRepositorio cantidadFormulaRepositorio;

    public CantidadFormula getCantidadFormulaById(long id) {
        CantidadFormula obj = cantidadFormulaRepositorio.findById(id).get();
        return obj;
    }

    public List<CantidadFormula> getAllCantidadFormula(){
        List<CantidadFormula> list = new ArrayList<>();
        cantidadFormulaRepositorio.findAll().forEach(e -> list.add(e));
        return list;
    }

    public synchronized boolean addCantidadFormula(CantidadFormula cantidadFormula) {
        List<CantidadFormula> list = cantidadFormulaRepositorio.findByIdAndPorcentaje(cantidadFormula.getId() , cantidadFormula.getPorcentaje());
        if (list.size() > 0) {
            return false;
        } else {
            cantidadFormulaRepositorio.save(cantidadFormula);
            return true;
        }

    }
    public void updateCantidadFormula(CantidadFormula cantidadFormula){
        cantidadFormulaRepositorio.save(cantidadFormula);
    }

    public void deleteCantidadFormula(int id){
        cantidadFormulaRepositorio.delete(getCantidadFormulaById(id));
    }

}
