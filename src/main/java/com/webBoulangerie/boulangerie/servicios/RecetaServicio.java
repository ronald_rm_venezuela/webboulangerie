package com.webBoulangerie.boulangerie.servicios;

import com.webBoulangerie.boulangerie.repositorios.RecetaRepositorio;
import com.webBoulangerie.boulangerie.data.entity.Receta;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecetaServicio  {

    @Autowired
    private RecetaRepositorio recetaRepositorio;


    public Receta getRecetaById(long id) {
        Receta obj = recetaRepositorio.findById(id).get();
        return obj;
    }


    public List<Receta> getAllReceta(){
        List<Receta> list = new ArrayList<>();
        recetaRepositorio.findAll().forEach(e -> list.add(e));
        return list;
    }


    public synchronized boolean addReceta(Receta receta) {
        List<Receta> list = recetaRepositorio.findByPesoUnitarioAndNombre(receta.getPesoUnitario(), receta.getNombre());
        if (list.size() > 0) {
            return false;
        } else {
            recetaRepositorio.save(receta);
            return true;
        }

    }

    public void updateReceta(Receta receta){
        recetaRepositorio.save(receta);
    }

    public void deleteReceta(int id){
        recetaRepositorio.delete(getRecetaById(id));
    }


}
