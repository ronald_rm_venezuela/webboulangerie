package com.webBoulangerie.boulangerie;

import com.webBoulangerie.boulangerie.data.entity.CantidadFormula;
import com.webBoulangerie.boulangerie.data.entity.Ingrediente;
import com.webBoulangerie.boulangerie.servicios.CantidadFormulaServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import java.util.List;

@RestController
public class IngredienteRest {

    @Autowired
    CantidadFormulaServicio cantidadFormulaServicio;

    @GetMapping("/cantidadFormula")
    public List<CantidadFormula> allCantidadFormula() {

        return cantidadFormulaServicio.getAllCantidadFormula();
    }

    @PostMapping("/cantidadFormula/post")
    public ResponseEntity<Void> addCantidadFormula(@RequestBody CantidadFormula cantidadFormula, UriComponentsBuilder builder) {
        boolean flag = cantidadFormulaServicio.addCantidadFormula(cantidadFormula);

        if (flag == false) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/cantidadFormula/{id}").buildAndExpand(cantidadFormula.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @PutMapping("/cantidadFormula/update") //para editar un in
    public ResponseEntity<CantidadFormula> updateReceta(@RequestBody CantidadFormula cantidadFormula) {
        cantidadFormulaServicio.updateCantidadFormula(cantidadFormula);
        return new ResponseEntity<CantidadFormula>(cantidadFormula, HttpStatus.OK);
    }

    @DeleteMapping("/cantidadFormula/{id}")
    public ResponseEntity<Void> deleteReceta(@PathVariable("id") Integer id) {
        cantidadFormulaServicio.deleteCantidadFormula(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
