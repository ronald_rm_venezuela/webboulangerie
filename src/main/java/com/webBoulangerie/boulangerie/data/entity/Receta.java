package com.webBoulangerie.boulangerie.data.entity;

  import javax.persistence.*;

@Entity
@Table(name = "receta")
public class Receta {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column (name = "id")
    private long id;

    @Column (name = "nombre")
    private String nombre;

    @Column (name = "pesoUnitario")
    private float pesoUnitario;

     public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPesoUnitario() {
        return pesoUnitario;
    }

    public void setPesoUnitario(float pesoUnitario) {
        this.pesoUnitario = pesoUnitario;
    }
}
