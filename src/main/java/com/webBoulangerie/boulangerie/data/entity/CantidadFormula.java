package com.webBoulangerie.boulangerie.data.entity;
    import org.springframework.boot.autoconfigure.web.ResourceProperties;

    import javax.persistence.*;

@Entity (name = "CantidadFormula")
public class CantidadFormula {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column (name = "id")
    private long id;

    @Column (name = "Porcentaje")
    private float porcentaje;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(float porcentaje) {
        this.porcentaje = porcentaje;
    }
}
