package com.webBoulangerie.boulangerie.data.help;

import java.util.ArrayList;
import java.lang.*;


public class CalculoIngrediente {

    private float pesoUnitario;
    private float cantidadPorciones;
    private ArrayList<Float> porcentaje;
    private ArrayList<Float> gramosIngrediente;

    public CalculoIngrediente(float pesoUnitario , float cantidadPorciones, ArrayList<Float> porcentaje){
        this.cantidadPorciones = cantidadPorciones;
        this.pesoUnitario = pesoUnitario;
        this.porcentaje = porcentaje;
    }


    public ArrayList<Float> calculoGramos(){

        float factorPanadero = 0;
        float porcentajeFormula = 0;
        gramosIngrediente = new ArrayList();
        float gramos = 0;


            for(int i = 0; i < porcentaje.size(); i++){

                porcentajeFormula += porcentaje.get(i);
            }

            factorPanadero = (cantidadPorciones*pesoUnitario)/porcentajeFormula;

            for(int j = 0; j < porcentaje.size(); j++) {

                gramos = porcentaje.get(j) * factorPanadero;
                gramosIngrediente.add(gramos);
            }

        return gramosIngrediente;

    }
}
