package com.webBoulangerie.boulangerie;

import com.webBoulangerie.boulangerie.data.entity.Receta;
        import com.webBoulangerie.boulangerie.servicios.RecetaServicio;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.*;
        import org.springframework.http.ResponseEntity;
        import org.springframework.web.util.UriComponentsBuilder;
        import org.springframework.http.HttpHeaders;
        import org.springframework.http.HttpStatus;


        import java.util.List;

@RestController
public class RecetaRest {

    @Autowired
    private RecetaServicio recetaServicio;


    @GetMapping("/recetas")
    public List<Receta> allrecetas() {

        return recetaServicio.getAllReceta();
    }

    @PostMapping("/receta/post")
    public ResponseEntity<Void> addReceta(@RequestBody Receta receta, UriComponentsBuilder builder) {
        boolean flag = recetaServicio.addReceta(receta);

        if (flag == false) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/receta/{id}").buildAndExpand(receta.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @PutMapping("/receta/update")
    public ResponseEntity<Receta> updateReceta(@RequestBody Receta receta) {
        recetaServicio.updateReceta(receta);
        return new ResponseEntity<Receta>(receta, HttpStatus.OK);
    }

    @DeleteMapping("/receta/{id}")
    public ResponseEntity<Void> deleteReceta(@PathVariable("id") Integer id) {
        recetaServicio.deleteReceta(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
