package com.webBoulangerie.boulangerie.controlador;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;
import com.webBoulangerie.boulangerie.data.entity.Receta;
import com.webBoulangerie.boulangerie.servicios.RecetaServicio;

@Controller
@RequestMapping("user")
public class RecetaControlador {

    @Autowired
    private RecetaServicio recetaServicio;
    @GetMapping("receta/{id}")
    public ResponseEntity<Receta> getRecetaById(@PathVariable("id") Integer id) {
        Receta receta = recetaServicio.getRecetaById(id);
        return new ResponseEntity<Receta>(receta, HttpStatus.OK);
    }

    @GetMapping("receta")
    public ResponseEntity<List<Receta>> getAllReceta() {
        List<Receta> list = recetaServicio.getAllReceta();
        return new ResponseEntity<List<Receta>>(list, HttpStatus.OK);
    }

    @PostMapping("receta")
    public ResponseEntity<Void> addReceta(@RequestBody Receta receta, UriComponentsBuilder builder) {
        boolean flag = recetaServicio.addReceta(receta);
        if (flag == false) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/receta/{id}").buildAndExpand(receta.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @PutMapping("receta")
    public ResponseEntity<Receta> updateReceta(@RequestBody Receta receta) {
        recetaServicio.updateReceta(receta);
        return new ResponseEntity<Receta>(receta, HttpStatus.OK);
    }
    @DeleteMapping("recetas/{id}")
    public ResponseEntity<Void> deleteReceta(@PathVariable("id") Integer id) {
        recetaServicio.deleteReceta(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
