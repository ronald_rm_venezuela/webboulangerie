package com.webBoulangerie.boulangerie.repositorios;

import com.webBoulangerie.boulangerie.data.entity.Receta;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface RecetaRepositorio extends CrudRepository<Receta, Long>{
    List<Receta> findByPesoUnitario(float pesoUnitario);
    List<Receta> findDistinctByNombre(String nombre);
    List<Receta> findByPesoUnitarioAndNombre(float pesoUnitario, String nombre);

}
