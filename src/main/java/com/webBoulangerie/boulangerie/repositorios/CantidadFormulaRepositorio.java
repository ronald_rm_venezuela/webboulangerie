package com.webBoulangerie.boulangerie.repositorios;

import org.springframework.data.repository.CrudRepository;
import com.webBoulangerie.boulangerie.data.entity.CantidadFormula;
import java.util.List;

public interface CantidadFormulaRepositorio extends CrudRepository <CantidadFormula, Long> {
    List<CantidadFormula> findByIdAndPorcentaje(long id , float porcentaje);
}
